<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */

$this->title = $model->proveedor;
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<style>
    .compras-view {
        text-align: justify;
        margin-left: 20px; /* Ajusta el margen izquierdo según sea necesario */
        margin-right: 20px; /* Ajusta el margen derecho según sea necesario */
    }

    .compras-view .detail-label {
        width: 30%; /* Ajusta el ancho según sea necesario */
        font-weight: bold; /* Opcional, para resaltar los nombres de los atributos */
    }

    .compras-view .detail-value {
        width: 70%; /* Ajusta el ancho según sea necesario */
    }
</style>

<div class="compras-view">

    <h1>Numero de compra: <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'concepto',
            'precio',
            'fecha',
            'proveedor',
        ],
        'options' => ['class' => 'table table-striped detail-view'], // Agrega clases de Bootstrap para una mejor apariencia si lo necesitas
        'template' => '<tr><th class="detail-label">{label}</th><td class="detail-value">{value}</td></tr>',
    ]) ?>

</div>
