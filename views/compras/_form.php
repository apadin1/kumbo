<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="compras-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'concepto')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el concepto', 'autocomplete' => 'on']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'precio')->textInput(['maxlength' => true, 'placeholder' => 'Ejemplo... 20.20', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'fecha')->textInput(['maxlength' => true, 'placeholder' => 'dd/mm/aaaa', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'proveedor')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el proveedor', 'autocomplete' => 'on']) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
