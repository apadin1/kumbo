<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'PDF Precio';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h2><?= Html::encode($this->title) ?></h2>

    <style>
        #precios {
            width: 100%;
            border-collapse: collapse;
        }

        #precios th, #precios td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        #precios th {
            background-color: #f2f2f2;
        }
    </style>

    <table id="precios">
        <tr>
            <th>Id</th>
            <th>Precio</th>
            <th>Objeto</th>
        </tr>

        <?php foreach ($dataProvider->getModels() as $model): ?>
            <tr>
                <td><?= $model->id ?></td>
                <td><?= $model->precio ?></td>
                <td><?= $model->idobjetivofinanciero ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
