<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Precios $model */

$this->title = 'Nuevo Precios';
$this->params['breadcrumbs'][] = ['label' => 'Precios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="precios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
