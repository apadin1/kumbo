<?php

use app\models\Precios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Precios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="precios-index container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="button-container">
        <?= Html::a('Nuevo Precio', ['create'], ['class' => 'btn btn-success btn-full-width']) ?>
        <!-- Botón para mostrar el GridView -->
        <?= Html::button('Mostrar Tabla', ['id' => 'show-gridview-btn', 'class' => 'btn btn-primary btn-full-width']) ?>
        <!-- Botón para PDF -->
        <?= Html::a('Descarga PDF', ['pdf'], ['class' => 'btn btn-danger btn-full-width', 'target' => '_blank']) ?>
        <!-- Botón para mostrar la imagen -->
        <?= Html::button('Ayuda', ['id' => 'show-image-btn', 'class' => 'btn btn-info btn-full-width']) ?>
 

    <!-- Nombre encima de la tabla -->
    <div class="container">


        <!-- Contenedor para el GridView que inicialmente está oculto -->
        <div id="gridview-container" style="display:none; margin-top: 20px;">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'columns' => [
                    'id',
                    'precio',
                    'idobjetivofinanciero',
                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                        }
                    ],
                ],
            ]);
            ?>
        </div>
        


        <div class="contenedor-principal">
    <div class="card">
        <h2>Precio Máximo:</h2>
        <p><?= $maxPrice ?> € </p>
    </div>

    <div class="card">
        <h2>Precio Mínimo:</h2>
        <p><?= $minPrice ?> € </p>
    </div>

    <div class="card">
        <h2>Precio Medio:</h2>
        <p><?= $avgPrice ?> €</p>
    </div>
</div>


        <!-- Contenedor para la imagen que inicialmente está oculto -->
        <div id="image-container" style="display:none; margin-top: 20px;">
            <img id="toggle-gridview-img" src="<?= Url::to('@web/imagenes/prueba.png') ?>" alt="Información" style="max-width: 100%; height: auto; cursor: pointer;">
        </div>



        <!-- Script para inicializar el gráfico -->
        <div>
            <canvas id="myChart"></canvas>
        </div>
    </div>
</div>
    </div>
