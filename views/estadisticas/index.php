<?php

use yii\helpers\Html;
use yii\helpers\Json;

$this->title = 'Estadísticas';
$this->params['breadcrumbs'][] = $this->title;

// Obtener los totales de ingresos, gastos, ventas y compras desde el controlador
$totalIngresos = $totales['ingresos'];
$totalGastos = $totales['gastos'];
$totalVentas = $totales['ventas'];
$totalCompras = $totales['compras'];

// Configurar los datos para el gráfico
$data = [
    'Ingresos' => $totalIngresos,
    'Gastos' => $totalGastos,
    'Ventas' => $totalVentas,
    'Compras' => $totalCompras,
];

// Convertir los datos a formato JSON
$dataJson = Json::encode($data);

?>

<div class="estadisticas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- Contenedor para el gráfico -->
    <canvas id="myChart"></canvas>

</div>

<?php
// Script para inicializar el gráfico
$this->registerJs("
    // Obtener el contexto del lienzo
    var ctx = document.getElementById('myChart').getContext('2d');

    // Crear el gráfico
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Ingresos', 'Gastos', 'Ventas', 'Compras'],
            datasets: [{
                label: 'Totales',
                data: $dataJson,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
");
?>
