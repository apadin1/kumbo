<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Compras;
use app\models\Objetivosfinancieros;

/** @var yii\web\View $this */
/** @var app\models\Gastos $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $comprasDisponibles */
/** @var array $objetivosFinancieros */

?>

<div class="gastos-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <!-- Desplegable para idobjetivofinanciero -->
             <?=
            $form->field($model, 'idobjetivofinanciero')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                            \app\models\Objetivosfinancieros::find()->all(),
                            'id',
                            function ($objetivofinanciero) {
                                return $objetivofinanciero->nombre;
                            }
                    ),
                    ['prompt' => 'Seleccione un objetivo financiero']
            )->label('Objetivo')
            ?>
        </div>
        <div class="col-md-6">
            <!-- Desplegable para idcompra -->
             <?=
            $form->field($model, 'idcompra')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                            \app\models\Compras::find()->all(),
                            'id',
                            function ($comprasDisponibles) {
                                return $comprasDisponibles->proveedor;
                            }
                    ),
                    ['prompt' => 'Selecciona una compra']
            )->label('Proveedor')
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <!-- Campo de entrada de texto para la fecha -->
            <?= $form->field($model, 'fecha')->textInput(['class' => 'form-control', 'placeholder' => 'dd/mm/aaaa']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'saldo')->textInput(['maxlength' => true, 'placeholder' => 'Ejemplo... 5.30', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'tipo')->dropDownList(['fijo' => 'Fijo', 'variable' => 'Variable'], ['prompt' => 'Selecciona tipo']) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <!-- Campo de entrada de texto para el IVA -->
            <?= $form->field($model, 'iva')->textInput(['maxlength' => true, 'placeholder' => 'Ejemplo... 20.20', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'concepto')->textInput(['maxlength' => true, 'placeholder' => 'Ejemplo... Luz, Agua', 'autocomplete' => 'off']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
