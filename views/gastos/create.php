<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Gastos $model */
/** @var array $comprasDisponibles */
/** @var array $objetivosFinancieros */

$this->title = 'Nuevo Gasto';
$this->params['breadcrumbs'][] = ['label' => 'Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gastos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'comprasDisponibles' => $comprasDisponibles, // Pasa $comprasDisponibles al formulario
        'objetivosFinancieros' => $objetivosFinancieros, // Pasa $objetivosFinancieros al formulario
    ]) ?>

</div>
