<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'PDF Gastos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h2><?= Html::encode($this->title) ?></h2>

    <style>
        #gastos {
            width: 100%;
            border-collapse: collapse;
        }

        #gastos th, #gastos td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        #gastos th {
            background-color: #f2f2f2;
        }
    </style>

    <table id="gastos">
        <tr>
            <th>Nª Gasto</th>
            <th>Saldo</th>
            <th>Fecha</th>
            <th>Concepto</th>
            <th>Tipo</th>
        </tr>

        <?php foreach ($dataProvider->getModels() as $model): ?>
            <tr>
                <td><?= $model->id ?></td>
                <td><?= $model->saldo ?>€</td>
                <td><?= $model->fecha ?></td>
                <td><?= $model->concepto ?></td>
                <td><?= $model->tipo ?></td>
                
            </tr>
        <?php endforeach; ?>
    </table>
</div>
