<?php

use app\models\Ventas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ventas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="button-container">
        <?= Html::a('Nuevo Presupuesto', ['create'], ['class' => 'btn btn-success btn-full-width']) ?>
        <!-- Botón para mostrar el GridView -->
        <?= Html::button('Mostrar Tabla', ['id' => 'show-gridview-btn', 'class' => 'btn btn-primary btn-full-width']) ?>
        <!-- Botón para PDF -->
        <?= Html::a('Descarga PDF', ['pdf'], ['class' => 'btn btn-danger btn-full-width', 'target' => '_blank']) ?>
        <!-- Botón para mostrar la imagen -->
        <?= Html::button('Ayuda', ['id' => 'show-image-btn', 'class' => 'btn btn-info btn-full-width']) ?>
    </div>

    <!-- Nombre encima de la tabla -->
    <div class="container">


        <!-- Contenedor para el GridView que inicialmente está oculto -->
        <div id="gridview-container" style="display:none; margin-top: 20px;">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'columns' => [
                    'saldo',
                    'fecha',
                    'concepto',
                    'plataforma',
                    'objeto',
                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                        }
                    ],
                ],
            ]);
            ?>
        </div>

        <!-- Contenedor para la imagen que inicialmente está oculto -->
        <div id="image-container" style="display:none; margin-top: 20px;">
            <img id="toggle-gridview-img" src="<?= Url::to('@web/imagenes/prueba1.png') ?>" alt="Información" style="max-width: 100%; height: auto; cursor: pointer;">
        </div>

        <!-- Script para inicializar el gráfico -->
        <div>
            <canvas id="myChart"></canvas>
        </div>
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                const ctx = document.getElementById('myChart').getContext('2d');

                // Utilizar los datos dinámicos proporcionados por PHP
                var datosGrafica = <?= $datosGrafica ?>;

                new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                        datasets: [{
                                label: 'Total de ventas por mes',
                                data: datosGrafica, // Usar los datos dinámicos aquí
                                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                                borderColor: 'rgba(54, 162, 235, 1)',
                                borderWidth: 3
                            }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            });
        </script>
    </div>
</div>
