<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ventas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ventas-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'saldo')->textInput(['maxlength' => true, 'placeholder' => 'Ejemplo... 5.30', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-4">
               <?= $form->field($model, 'fecha')->textInput(['maxlength' => true, 'placeholder' => 'dd/mm/aaaa', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'objeto')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el concepto', 'autocomplete' => 'on']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'concepto')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el concepto', 'autocomplete' => 'on']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'plataforma')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca la plataforma de venta', 'autocomplete' => 'on']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>






    <?php ActiveForm::end(); ?>

</div>
