<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'PDF Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h2><?= Html::encode($this->title) ?></h2>

    <style>
        #ventas {
            width: 100%;
            border-collapse: collapse;
        }

        #ventas th, #ventas td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        #ventas th {
            background-color: #f2f2f2;
        }
    </style>

    <table id="ventas">
        <tr>
            <th>Nª Venta</th>
            <th>Saldo</th>
            <th>Fecha</th>
            <th>Concepto</th>
            <th>Plataforma</th>
            <th>Objeto</th>
        </tr>

        <?php foreach ($dataProvider->getModels() as $model): ?>
            <tr>
                <td><?= $model->id ?></td>
                <td><?= $model->saldo ?>€</td>
                <td><?= $model->fecha ?></td>
                <td><?= $model->concepto ?></td>
                <td><?= $model->plataforma ?></td>
                <td><?= $model->objeto ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
