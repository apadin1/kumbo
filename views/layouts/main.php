<?php

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

// Calcula el saldo total
$totalVentas = \app\models\Ventas::find()->sum('saldo');
$totalIngresos = \app\models\Ingresos::find()->sum('saldo');
$totalGastos = \app\models\Gastos::find()->sum('saldo');
$totalCompras = \app\models\Compras::find()->sum('precio');
$saldototal = $totalVentas - $totalVentas + $totalIngresos - $totalGastos - $totalCompras + $totalCompras;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>

        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?= $this->registerCsrfMetaTags() ?>
        <title><?= Yii::$app->name ?></title>
        <?php $this->head() ?>
        <!-- Enlace al archivo JavaScript de Chart.js -->
        <?= Html::script('https://cdn.jsdelivr.net/npm/chart.js') ?>

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?= Url::to('@web/logo.ico') ?>" />
        <link rel="icon" type="imagenes/x-icon" href="<?= Url::to('@web/logo.ico') ?>"/>
    </head>
    <body class="d-flex flex-column h-100">
        <?php $this->beginBody() ?>

        <header>
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav mr-auto'],
                'items' => [
                    ['label' => 'Objetivos', 'url' => ['/objetivosfinancieros/index']],
                    ['label' => 'Compras', 'url' => ['/compras/index']],
                    ['label' => 'Ventas', 'url' => ['/ventas/index']],
                    ['label' => 'Ingresos', 'url' => ['/ingresos/index']],
                    ['label' => 'Gastos', 'url' => ['/gastos/index']],
                ],
            ]);

            // Mostrar el saldo en el header
            echo '<span class="navbar-text mr-3">Tu Saldo: ' . Yii::$app->formatter->asCurrency($saldototal) . ' €</span>';

            NavBar::end();
            ?>
        </header>

        <main role="main" class="flex-shrink-0">
            <div class="container contenedor-fondo">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </main>

        <footer class="footer mt-auto py-3 text-muted">
            <div class="container">
                <div class="footer-content">
                    <p>&copy; Derechos de autor de Andrés Padín</p>
                    <p>&copy; Nuestra Empresa Kumbo S.L. <?= date('Y') ?></p>
                </div>
            </div>
        </footer>


<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
