<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Objetivos Financieros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objetivosfinancieros-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'objeto',
        ],
    ]) ?>

    <?= DetailView::widget([
        'model' => $modelPrecio,
        'attributes' => [
            'precio',
        ],
    ]) ?>

</div>
