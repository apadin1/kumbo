<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Objetivosfinancieros $model */
/** @var app\models\Precios $modelPrecio */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="objetivosfinancieros-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el nombre', 'autocomplete' => 'on']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'objeto')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el objeto', 'autocomplete' => 'on']) ?>
        </div>
    </div>

    <?= $form->field($modelPrecio, 'precio')->textInput(['maxlength' => true, 'placeholder' => 'Ejemplo... 20.20', 'autocomplete' => 'off']) ?>

     <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>



    <?php ActiveForm::end(); ?>

</div>
