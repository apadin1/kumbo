<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Objetivosfinancieros $model */

$this->title = 'Crear nuevo Objetivos ';
$this->params['breadcrumbs'][] = ['label' => 'Objetivosfinancieros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="objetivosfinancieros-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPrecio' => $modelPrecio, // Asegúrate de pasar la variable $modelPrecio aquí
    ]) ?>

</div>
