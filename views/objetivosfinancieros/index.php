<?php

// Importar las clases necesarias de Yii para generar HTML, URLs y GridView
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

// Configuración del título de la página y migas de pan (breadcrumbs)
$this->title = 'Objetivosfinancieros'; // Título de la página
$this->params['breadcrumbs'][] = $this->title; // Agregar título a las migas de pan
?>
<div class="tarjetaobjetivos objetivosfinanciero mb-4">
    <h1>Objetivos</h1>
    <p>
        <?= Html::a('<i class="bi bi-plus"></i> Nuevo Objetivo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div id="objetivos-container" class="row">
        <?php foreach ($dataProvider->getModels() as $model): ?>
            <div class="col-md-4">
                <!-- Agregar la estructura de la tarjeta aquí -->
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title"><?= Html::encode($model->nombre) ?></h5>
                        <p><strong> </strong></p>
                        <h5 class="card-title">Objeto :</h5>
                        <p class="card-text"><?= Html::encode($model->objeto) ?></p>
                        <?php if (!empty($model->precios)): ?>

                            <ul>
                                <?php foreach ($model->precios as $precio): ?>
                                    <h5> Precio: <?= $precio->precio ?> €</h5>
                                <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            <p>No hay precios asociados.</p>
                        <?php endif; ?>
                        <div class="button-group">
                            <?= Html::a('<i class="fa-solid fa-file-pen"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-sm']) ?>
                            <?= Html::a('<i class="fa-solid fa-trash-can"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data' => ['confirm' => '¿Estás seguro de eliminar este elemento?', 'method' => 'post',],]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
