<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Gastos;
use app\models\Ingresos;

AppAsset::register($this);
$saldototal = $totalVentas - $totalVentas + $totalIngresos - $totalGastos - $totalCompras + $totalCompras;

$this->title = 'My Yii Application';

// Registro de AppAsset
\app\assets\AppAsset::register($this);
?>
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= Html::encode($this->title) ?></title>
        <?= Html::cssFile('@web/css/site.css') ?>
        <link rel="shortcut icon" href="<?= Url::to('@web/logo.ico') ?>" />
        <link rel="icon" type="imagenes/x-icon" href="<?= Url::to('@web/logo.ico') ?>"/>
    </head>

    <body>
        <div class="contenido-principal">
            <!-- IMAGEN DE FONDO -->
            <div class="fondo-pantalla">
                <?=
                Html::img('@web/imagenes/edificio.jpg', [
                    'alt' => 'Edificios - oficinas',
                    'class' => 'img-fluid'
                ])
                ?>
            </div>

            <div class="info-principal hero">
                <h2>Descubre Kumbo</h2>
                <p class="texto-principal">
                    Estamos aquí para ayudarte a alcanzar tus metas financieras. Con nuestras herramientas intuitivas y orientación experta, podrás ahorrar y hacer crecer tu capital de manera fácil y segura.
                    Uniendote a nosotros y comencemos juntos este emocionante viaje hacia un solvente futuro financiero sólido.
                </p>
                <p class="texto-principal">
                    ¡Vamos a empezar a ahorrar y construir juntos!
                </p>

                <!-- Tarjeta de Credito -->
                <div class="card-custom">
                    <div class="card__side-custom card__side_front-custom">
                        <div class="flex__1-custom">
                            <p class="card__side__name-bank-custom">Tarjeta Kumbo</p>
                            <div class="card__side__chip-custom"></div>
                            <p class="card__side__name-person-custom">Kumbo Bank</p>
                            <p class="navbar-text mr-3">XXXXXXXXXXXX €</p>
                        </div>
                    </div>
                    <div class="card__side-custom card__side_back-custom">
                        <div class="card__side__black-custom"></div>
                        <p class="card__side__number">Saldo: /p>
                        <p> </p>
                        <!-- Reemplaza "XXXX XXXX XXXX XXXX" con la consulta de tu saldo -->
                        <?= '<p class="card__side__number-custom">' . Yii::$app->formatter->asCurrency($saldototal) . ' €</p>' ?>
                        <div class="flex__2-custom">
                            <p class="card__side__other-numbers-custom card__side__other-numbers_1-custom"></p>
                            <p class="card__side__other-numbers-custom card__side__other-numbers_2-custom"></p>
                            <div class="card__side__photo-custom"></div>
                            <div class="card__side__debit-custom">Saldo</div>
                        </div>
                        <p class="card__side__other-info-custom">

                        </p>
                    </div>
                </div>

                <p class="card__side__other-info-custom">

                </p>
            </div>
        </div>


    </div>

    <div><!-- comment -->
        <p> </p>
    </div>


    <!-- Contenedor principal para el gráfico y las tarjetas -->
    <div class="contenedor-principal">
        <!-- Canvas para el gráfico -->
        <div class="chart-container">
            <canvas id="myChart"></canvas>
        </div>


        <!-- Contenedor de tarjetas -->
        <div class="contenedor">
            <div class="tarjeta">
                <h2 class="titulo"> Último Objetivo</h2>
                <div class="cuerpo">
                    <?=
                    Html::img(
                            '@web/imagenes/objetivo.png',
                            [
                                'alt' => 'obejivos - objetos',
                                'class' => 'img-fluid '
                            ]
                    );
                    ?>
                    <p>
                        
                    </p>
                    <p>Nombre: <?= $ultimoObjetivo->nombre ?></p>
                    <p>Objeto: <?= $ultimoObjetivo->objeto ?></p>
                </div>
                <?= Html::a('Más información', ['/objetivosfinancieros/index'], ['class' => 'btn btn-primary']) ?>
            </div>

            <div class="tarjeta">
                <h2 class="titulo">Mayor Gasto</h2>
                <div class="cuerpo">
                    <?php
                    $mayorGasto = Gastos::find()->orderBy(['saldo' => SORT_DESC])->limit(1)->one();
                    if ($mayorGasto !== null) :
                        ?>
                        <?=
                        Html::img(
                                '@web/imagenes/iva.png',
                                [
                                    'alt' => 'Mayor Gasto',
                                    'class' => 'img-fluid'
                                ]
                        );
                        ?>
                    <p>
                        
                    </p>
                        <p><?= Yii::$app->formatter->asCurrency($mayorGasto->saldo) ?> €</p>
                    <?php else : ?>
                        <p>No se encontraron gastos.</p>
                    <?php endif; ?>
                </div>
                <?= Html::a('Más información', ['/gastos/index'], ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="tarjeta">
                <h2 class="titulo">Mayor Ingreso</h2>
                <div class="cuerpo">
                    <?php
                    $mayorIngreso = Ingresos::obtenerMayorIngreso();
                    if ($mayorIngreso !== null) :
                        ?>
                        <?=
                        Html::img(
                                '@web/imagenes/venta.png',
                                [
                                    'alt' => 'obejivos - objetos',
                                    'class' => 'img-fluid '
                                ]
                        );
                        ?>
                    <p>
                        
                    </p> <!-- Mostrar detalles del mayor ingreso -->
                        <p><?= Yii::$app->formatter->asCurrency($mayorIngreso->saldo) ?> €</p>
                    <?php else : ?>
                        <p>No se encontraron ingresos.</p>
                    <?php endif; ?>
                         
                </div>
                <?= Html::a('Más información', ['/ingresos/index'], ['class' => 'btn btn-primary']) ?>
            </div>




        </div>
    </div>


    <!-- Script para inicializar el gráfico-->
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const ctx = document.getElementById('myChart').getContext('2d');

            const data = {
                labels: [
                    'Total Ventas',
                    'Total Ingresos',
                    'Total Gastos',
                    'Total Compras'
                ],
                datasets: [{
                        label: 'My First Dataset',
                        data: [<?= $totalVentas ?>, <?= $totalIngresos ?>, <?= $totalGastos ?>, <?= $totalCompras ?>],
                        backgroundColor: [
                            'rgb(46, 204, 113)',
                            'rgb(25, 111, 61)',
                            'rgb(176, 58, 46)',
                            'rgb(231, 76, 60)'
                        ]
                    }]
            };

            const config = {
                type: 'pie',
                data: data,
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top'
                        },
                        tooltip: {
                            callbacks: {
                                label: function (tooltipItem) {
                                    return tooltipItem.label + ': ' + tooltipItem.raw;
                                }
                            }
                        }
                    }
                }
            };

            new Chart(ctx, config);
        });
    </script>

    <div><!-- comment -->
        <p> </p>
    </div>
</div>
</body>

</html>
