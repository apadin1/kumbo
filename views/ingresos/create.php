<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ingresos $model */
/** @var array $ventasDisponibles */
/** @var array $objetivosFinancieros */ // Agrega esta línea para definir la variable $objetivosFinancieros

$this->title = 'Nuevo Ingreso';
$this->params['breadcrumbs'][] = ['label' => 'Ingresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingresos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ventasDisponibles' => $ventasDisponibles,
        'objetivosFinancieros' => $objetivosFinancieros, // Pasa la variable $objetivosFinancieros al formulario
    ]) ?>

</div>
