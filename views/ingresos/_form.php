<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ingresos $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $ventasDisponibles */
?>

<div class="ingresos-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?=
            $form->field($model, 'idobjetivofinanciero')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                            \app\models\Objetivosfinancieros::find()->all(),
                            'id',
                            function ($objetivofinanciero) {
                                return $objetivofinanciero->nombre;
                            }
                    ),
                    ['prompt' => 'Seleccione un objetivo financiero']
            )->label('Objetivo')
            ?>

        </div>
        <div class="col-md-6">

            <?=
            $form->field($model, 'idventa')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                            \app\models\Ventas::find()->all(),
                            'id',
                            function ($venta) {
                                return $venta->concepto;
                            }
                    ),
                    ['prompt' => 'Seleccione una venta']
            )->label('Venta')
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'saldo')->textInput(['maxlength' => true, 'placeholder' => 'Ejemplo... 5.30', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'fecha')->textInput(['maxlength' => true, 'placeholder' => 'dd/mm/aaaa', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-4">
<?= $form->field($model, 'emisor')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el emisor', 'autocomplete' => 'off']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
<?= $form->field($model, 'concepto')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el concepto', 'autocomplete' => 'off']) ?>
        </div>
    </div>

    <div class="form-group">
    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
