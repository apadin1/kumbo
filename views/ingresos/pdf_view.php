<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'PDF Ingresos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h2><?= Html::encode($this->title) ?></h2>

    <style>
        #compras {
            width: 100%;
            border-collapse: collapse;
        }

        #compras th, #compras td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        #compras th {
            background-color: #f2f2f2;
        }
    </style>

    <table id="compras">
        <tr>
            <th>Nª Compra</th>
            <th>Saldo</th>
            <th>Fecha</th>
            <th>Concepto</th>
            <th>Emisor</th>
        </tr>

        <?php foreach ($dataProvider->getModels() as $model): ?>
            <tr>
                <td><?= $model->id ?></td>
                <td><?= $model->saldo ?> €</td>
                <td><?= $model->fecha ?></td>
                <td><?= $model->concepto ?></td>
                <td><?= $model->emisor ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
