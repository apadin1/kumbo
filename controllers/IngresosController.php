<?php

namespace app\controllers;

use Yii;
use app\models\Ingresos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\IngresosSearch;
use app\models\Ventas;
use app\models\ObjetivosFinancieros;
use app\models\Precios;
use yii\helpers\ArrayHelper;

/**
 * IngresosController implements the CRUD actions for Ingresos model.
 */
class IngresosController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Ingresos models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Ingresos::find(),
        ]);

        // Consultas para obtener el total de compras de cada mes
        $totalEnero = Ingresos::find()->where(['MONTH(fecha)' => 1])->sum('saldo');
        $totalFebrero = Ingresos::find()->where(['MONTH(fecha)' => 2])->sum('saldo');
        $totalMarzo = Ingresos::find()->where(['MONTH(fecha)' => 3])->sum('saldo');
        $totalAbril = Ingresos::find()->where(['MONTH(fecha)' => 4])->sum('saldo');
        $totalMayo = Ingresos::find()->where(['MONTH(fecha)' => 5])->sum('saldo');
        $totalJunio = Ingresos::find()->where(['MONTH(fecha)' => 6])->sum('saldo');
        $totalJulio = Ingresos::find()->where(['MONTH(fecha)' => 7])->sum('saldo');
        $totalAgosto = Ingresos::find()->where(['MONTH(fecha)' => 8])->sum('saldo');
        $totalSeptiembre = Ingresos::find()->where(['MONTH(fecha)' => 9])->sum('saldo');
        $totalOctubre = Ingresos::find()->where(['MONTH(fecha)' => 10])->sum('saldo');
        $totalNoviembre = Ingresos::find()->where(['MONTH(fecha)' => 11])->sum('saldo');
        $totalDiciembre = Ingresos::find()->where(['MONTH(fecha)' => 12])->sum('saldo');

        // Array con los totales de compras de cada mes
        $datosGrafica = [
            $totalEnero,
            $totalFebrero,
            $totalMarzo,
            $totalAbril,
            $totalMayo,
            $totalJunio,
            $totalJulio,
            $totalAgosto,
            $totalSeptiembre,
            $totalOctubre,
            $totalNoviembre,
            $totalDiciembre,
        ];

        return $this->render('index', [
                    'dataProvider' => new ActiveDataProvider([
                        'query' => Ingresos::find(),
                            ]),
                    'datosGrafica' => json_encode($datosGrafica), // Convertir el array a JSON
        ]);

        /*
          $searchModel = new ComprasSearch();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          return $this->render('index',[
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          ]);
         * 
         * */
    }

    /**
     * Displays a single Ingresos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ingresos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Ingresos();
        $ventasDisponibles = Ventas::getVentasDisponibles(); // Aquí obtienes las ventas disponibles
        // Obtener todos los objetivos financieros disponibles
        $objetivosFinancieros = Objetivosfinancieros::find()->all();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
                    'ventasDisponibles' => $ventasDisponibles,
                    'objetivosFinancieros' => ArrayHelper::map($objetivosFinancieros, 'id', 'nombre'), // Utiliza ArrayHelper::map para convertir los modelos en un arreglo clave-valor
        ]);
    }

    /**
     * Updates an existing Ingresos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelPrecio = Precios::findOne(['idobjetivofinanciero' => $id]);

        // Obtener todos los objetivos financieros disponibles
        $objetivosFinancieros = Objetivosfinancieros::find()->all();

        // Lógica para obtener $ventasDisponibles
        $ventasDisponibles = Ventas::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->save() && $modelPrecio->load(Yii::$app->request->post()) && $modelPrecio->save()) {
            // Redirige a la ruta deseada
            return $this->redirect(['/compras/create']);
        }



        return $this->render('update', [
                    'model' => $model,
                    'ventasDisponibles' => $ventasDisponibles,
                    'objetivosFinancieros' => $objetivosFinancieros,
        ]);
    }

    /**
     * Deletes an existing Ingresos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ingresos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Ingresos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Ingresos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPdf() {
        $searchModel = new IngresosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $html = $this->renderPartial('pdf_view', ['dataProvider' => $dataProvider]);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->showImageErrors = true;
        $mpdf->SetDisplayMode('fullpage', 'two');
        $mpdf->list_indent_first_level = 0;
        $mpdf->writeHTML($html);
        $mpdf->Output();
        exit;
    }

}
