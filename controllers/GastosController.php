<?php

namespace app\controllers;

use Yii;
use app\models\Gastos;
use app\models\Compras;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\GastosSearch;
use app\models\ObjetivosFinancieros;
use yii\helpers\ArrayHelper;

/**
 * GastosController implements the CRUD actions for Gastos model.
 */
class GastosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Gastos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Gastos::find(),
        ]);

        // Consultas para obtener el total de compras de cada mes
        $totalEnero = Gastos::find()->where(['MONTH(fecha)' => 1])->sum('saldo');
        $totalFebrero = Gastos::find()->where(['MONTH(fecha)' => 2])->sum('saldo');
        $totalMarzo = Gastos::find()->where(['MONTH(fecha)' => 3])->sum('saldo');
        $totalAbril = Gastos::find()->where(['MONTH(fecha)' => 4])->sum('saldo');
        $totalMayo = Gastos::find()->where(['MONTH(fecha)' => 5])->sum('saldo');
        $totalJunio = Gastos::find()->where(['MONTH(fecha)' => 6])->sum('saldo');
        $totalJulio = Gastos::find()->where(['MONTH(fecha)' => 7])->sum('saldo');
        $totalAgosto = Gastos::find()->where(['MONTH(fecha)' => 8])->sum('saldo');
        $totalSeptiembre = Gastos::find()->where(['MONTH(fecha)' => 9])->sum('saldo');
        $totalOctubre = Gastos::find()->where(['MONTH(fecha)' => 10])->sum('saldo');
        $totalNoviembre = Gastos::find()->where(['MONTH(fecha)' => 11])->sum('saldo');
        $totalDiciembre = Gastos::find()->where(['MONTH(fecha)' => 12])->sum('saldo');

        // Array con los totales de compras de cada mes
        $datosGrafica = [
            $totalEnero,
            $totalFebrero,
            $totalMarzo,
            $totalAbril,
            $totalMayo,
            $totalJunio,
            $totalJulio,
            $totalAgosto,
            $totalSeptiembre,
            $totalOctubre,
            $totalNoviembre,
            $totalDiciembre,
        ];

        // Llama a la función para obtener los datos del gráfico de objetivos financieros
        $datosObjetivosGrafica = $this->obtenerDatosObjetivosGrafica();
        
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Gastos::find(),
            ]),
            'datosGrafica' => json_encode($datosGrafica), // Convertir el array a JSON
            'datosObjetivosGrafica' => json_encode($datosObjetivosGrafica),
        ]);
    }

    /**
     * Displays a single Gastos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gastos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
{
    $model = new Gastos();
    $comprasDisponibles = Compras::find()->select(['concepto', 'id'])->indexBy('id')->column();
    $objetivosFinancieros = Objetivosfinancieros::find()->select(['nombre', 'id'])->indexBy('id')->column();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
    }

    return $this->render('create', [
        'model' => $model,
        'comprasDisponibles' => $comprasDisponibles,
        'objetivosFinancieros' => $objetivosFinancieros,
    ]);
}


    /**
     * Updates an existing Gastos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Gastos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gastos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Gastos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gastos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Generates a PDF for the Gastos models.
     */
    public function actionPdf()
    {
        $searchModel = new GastosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $html = $this->renderPartial('pdf_view', ['dataProvider' => $dataProvider]);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->showImageErrors = true;
        $mpdf->SetDisplayMode('fullpage', 'two');
        $mpdf->list_indent_first_level = 0;
        $mpdf->writeHTML($html);
        $mpdf->Output();
        exit;
    }

    /**
     * Fetch data for financial goals chart.
     * @return array
     */
    protected function obtenerDatosObjetivosGrafica()
    {
        // Obtener los objetivos financieros
        $objetivos = ObjetivosFinancieros::find()->all();

        // Consultar el total de gastos de cada mes, agrupados por objetivos financieros
        $totalGastosPorMes = Gastos::find()
            ->select(['idobjetivofinanciero', 'MONTH(fecha) as mes', 'SUM(saldo) as total'])
            ->groupBy(['idobjetivofinanciero', 'MONTH(fecha)'])
            ->asArray()
            ->all();

        // Crear un array con los datos para la gráfica de objetivos financieros
        $datosGrafica = [];
        foreach ($objetivos as $objetivo) {
            foreach ($totalGastosPorMes as $gasto) {
                if ($gasto['idobjetivofinanciero'] == $objetivo->id) {
                    $datosGrafica[] = [
                        'idobjetivofinanciero' => $objetivo->id,
                        'nombre' => $objetivo->nombre,
                        'mes' => $gasto['mes'],
                        'gasto' => $gasto['total'],
                        'objetivo' => $objetivo->objeto,
                    ];
                }
            }
        }

        return $datosGrafica;
    }
}
