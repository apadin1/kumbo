<?php

namespace app\controllers;
use Yii;
use app\models\Compras;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ComprasSearch;

/**
 * ComprasController implements the CRUD actions for Compras model.
 */
class ComprasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Compras models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Compras::find(),
            
        ]);

        // Consultas para obtener el total de compras de cada mes
        $totalEnero = Compras::find()->where(['MONTH(fecha)' => 1])->sum('precio');
        $totalFebrero = Compras::find()->where(['MONTH(fecha)' => 2])->sum('precio');
        $totalMarzo = Compras::find()->where(['MONTH(fecha)' => 3])->sum('precio');
        $totalAbril = Compras::find()->where(['MONTH(fecha)' => 4])->sum('precio');
        $totalMayo = Compras::find()->where(['MONTH(fecha)' => 5])->sum('precio');
        $totalJunio = Compras::find()->where(['MONTH(fecha)' => 6])->sum('precio');
        $totalJulio = Compras::find()->where(['MONTH(fecha)' => 7])->sum('precio');
        $totalAgosto = Compras::find()->where(['MONTH(fecha)' => 8])->sum('precio');
        $totalSeptiembre = Compras::find()->where(['MONTH(fecha)' => 9])->sum('precio');
        $totalOctubre = Compras::find()->where(['MONTH(fecha)' => 10])->sum('precio');
        $totalNoviembre = Compras::find()->where(['MONTH(fecha)' => 11])->sum('precio');
        $totalDiciembre = Compras::find()->where(['MONTH(fecha)' => 12])->sum('precio');

        // Array con los totales de compras de cada mes
        $datosGrafica = [
            $totalEnero,
            $totalFebrero,
            $totalMarzo,
            $totalAbril,
            $totalMayo,
            $totalJunio,
            $totalJulio,
            $totalAgosto,
            $totalSeptiembre,
            $totalOctubre,
            $totalNoviembre,
            $totalDiciembre,
        ];
        
        
        return $this->render('index', [
    'dataProvider' => new ActiveDataProvider([
        'query' => Compras::find(),
    ]),
    'datosGrafica' => json_encode($datosGrafica), // Convertir el array a JSON
        ]);
    

    

         
        /*
        $searchModel = new ComprasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
         * 
         **/
    }

    /**
     * Displays a single Compras model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Compras model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
   public function actionCreate()
    {
        $model = new Compras();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    
    /**
     * Updates an existing Compras model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Compras model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Compras model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Compras the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Compras::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    //boyon de ver 
     public function actionVer()
    {
        $model = new Compras();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    // boton de pdf aun no funciona.
    

  public function actionPdf()
{
    $searchModel = new ComprasSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    $html = $this->renderPartial('pdf_view', ['dataProvider' => $dataProvider]);
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->showImageErrors = true;
    $mpdf->SetDisplayMode('fullpage', 'two');
    $mpdf->list_indent_first_level = 0;
    $mpdf->writeHTML($html);
    $mpdf->Output();
    exit;
}

 public function actionTransferirIvas()
    {
        // Obtener todas las compras que necesitan transferirse
        $compras = Compras::find()->all();

        // Transferir los IVAs a los gastos
        foreach ($compras as $compra) {
            $gasto = new Gastos();
            $gasto->iva = $compra->iva21 + $compra->iva10 + $compra->iva4;
            // Otros campos que necesites transferir
            // $gasto->campo = $compra->campo;

            if (!$gasto->save()) {
                Yii::error("Error al transferir el IVA de la compra con ID: {$compra->id}");
                // Manejar el error según sea necesario
            }
        }

        Yii::$app->session->setFlash('success', 'IVAs transferidos exitosamente a los gastos.');

        return $this->redirect(['index']); // Redirigir a la página de inicio de compras
    }
    
}

