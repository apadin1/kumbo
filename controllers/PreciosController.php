<?php

namespace app\controllers;

use Yii;
use app\models\Precios;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PreciosSearch;

/**
 * PreciosController implements the CRUD actions for Precios model.
 */
class PreciosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Precios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        // Consulta para obtener el precio máximo
        $maxPrice = Precios::find()->max('precio');

        // Consulta para obtener el precio mínimo
        $minPrice = Precios::find()->min('precio');

        // Consulta para obtener el precio medio
        $avgPrice = Precios::find()
            ->select(new \yii\db\Expression('ROUND(AVG(precio), 2)'))
            ->scalar();

        // Aquí puedes seguir con la creación de tu ActiveDataProvider para mostrar los datos en la vista
        $dataProvider = new ActiveDataProvider([
            'query' => Precios::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'maxPrice' => $maxPrice,
            'minPrice' => $minPrice,
            'avgPrice' => $avgPrice,
        ]);
    }

    /**
     * Displays a single Precios model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Precios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Precios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Precios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
   public function actionUpdate($id)
{
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        Yii::$app->session->setFlash('success', 'El precio se ha actualizado correctamente.');
        return $this->redirect(['view', 'id' => $model->id]);
    }

    return $this->render('update', [
        'model' => $model,
    ]);
}

    /**
     * Deletes an existing Precios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Precios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Precios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Precios::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPdf()
    {
        $searchModel = new PreciosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $html = $this->renderPartial('pdf_view', ['dataProvider' => $dataProvider]);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->showImageErrors = true;
        $mpdf->SetDisplayMode('fullpage', 'two');
        $mpdf->list_indent_first_level = 0;
        $mpdf->writeHTML($html);
        $mpdf->Output();
        exit;
    }
}

