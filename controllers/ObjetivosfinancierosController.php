<?php

namespace app\controllers;

use Yii;
use app\models\Objetivosfinancieros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Precios;

class ObjetivosfinancierosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Objetivosfinancieros::find()->with('precios'),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelPrecio = Precios::findOne(['idobjetivofinanciero' => $id]);

        return $this->render('view', [
            'model' => $model,
            'modelPrecio' => $modelPrecio,
        ]);
    }

    public function actionCreate()
    {
        $model = new Objetivosfinancieros();
        $modelPrecio = new Precios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // Establecer la relación entre el objetivo financiero y el precio
            $modelPrecio->idobjetivofinanciero = $model->id;
            if($modelPrecio->load(Yii::$app->request->post()) && $modelPrecio->save()) {
                $dataProvider = new ActiveDataProvider([
                    'query' => Objetivosfinancieros::find()->with('precios'),
                ]);
                return $this->redirect(['index', 'dataProvider' => $dataProvider]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelPrecio' => $modelPrecio,
        ]);
    }

  public function actionUpdate($id)
{
    $model = $this->findModel($id);
    $modelPrecio = Precios::findOne(['idobjetivofinanciero' => $id]);

    if ($model->load(Yii::$app->request->post()) && $model->save() && $modelPrecio->load(Yii::$app->request->post()) && $modelPrecio->save()) {
        // Redirige a la ruta deseada
        return $this->redirect(['/compras/create']); 
    }

    return $this->render('update', [
        'model' => $model,
        'modelPrecio' => $modelPrecio,
    ]);
}




    public function actionDelete($id)
    {
        // Encuentra el modelo de objetivo financiero
        $objetivo = $this->findModel($id);

        // Encuentra y elimina todos los precios asociados
        $precios = Precios::deleteAll(['idobjetivofinanciero' => $id]);

        // Elimina el objetivo financiero
        $objetivo->delete();

        // Redirige a la página de índice
        return $this->redirect(['index']);
    }

    public function actionAddCard()
    {
        $model = new Objetivosfinancieros();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('add-card', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Objetivosfinancieros::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('El modelo no se pudo encontrar.');
        }
    }
}
