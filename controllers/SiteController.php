<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ventas;
use app\models\Ingresos;
use app\models\Gastos;
use app\models\Compras;
use app\models\Objetivosfinancieros;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        // Calcula la suma de ventas
        $totalVentas = Ventas::find()->sum('saldo');

        // Calcula la suma de ingresos
        $totalIngresos = Ingresos::find()->sum('saldo');

        // Calcula la suma de gastos
        $totalGastos = Gastos::find()->sum('saldo');

        // Calcula la suma de compras
        $totalCompras = Compras::find()->sum('precio');

        // Calcula el saldo total
        $saldototal = $totalVentas + $totalIngresos - $totalGastos - $totalCompras;

        // Llama a la función sumarIvas() en el modelo Compras para obtener el total de los IVAs
        $totalIvas = Compras::sumarIvas();

        // Consulta para obtener el último registro de la tabla objetivosfinancieros
        $ultimoObjetivo = Objetivosfinancieros::find()->orderBy(['id' => SORT_DESC])->one();

        // Consulta para obtener el saldo más alto de la tabla ventas
        $mayorGasto = Gastos::obtenerMayorGasto();

        $mayorIngreso = Ingresos::obtenerMayorIngreso();

        // Verifica si se encontró algún registro en objetivosfinancieros
        if ($ultimoObjetivo !== null) {
            // Muestra los valores de nombre y objeto
            echo "Último nombre: " . $ultimoObjetivo->nombre . "<br>";
            echo "Último objeto: " . $ultimoObjetivo->objeto . "<br>";
        } else {
            echo "No se encontraron registros en la tabla objetivosfinancieros.";
        }

        // Renderiza la vista index con los datos calculados y consultados
        return $this->render('index', [
                    'saldo' => $saldototal,
                    'totalVentas' => $totalVentas,
                    'totalIngresos' => $totalIngresos,
                    'totalGastos' => $totalGastos,
                    'totalCompras' => $totalCompras,
                    'ultimoObjetivo' => $ultimoObjetivo,
                    'totalIvas' => $totalIvas,
                    'mayorGasto' => $mayorGasto, // Asegúrate de pasar la variable correcta aquí
                    'mayorIngreso' => $mayorIngreso,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}
