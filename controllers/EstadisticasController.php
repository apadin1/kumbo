<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Estadisticas; // Importa el modelo Estadisticas

class EstadisticasController extends Controller
{
    public function actionIndex()
    {
        $totales = Estadisticas::getTotales(); // Usa Estadisticas::getTotales() para llamar al método
        return $this->render('index', [
            'totales' => $totales,
        ]);
    }
}
