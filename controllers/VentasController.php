<?php

namespace app\controllers;

use Yii;
use app\models\Ventas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\VentasSearch;



/**
 * VentasController implements the CRUD actions for Ventas model.
 */
class VentasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Ventas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ventas::find(),
            
        ]);

        // Consultas para obtener el total de compras de cada mes
        $totalEnero = Ventas::find()->where(['MONTH(fecha)' => 1])->sum('saldo');
        $totalFebrero = Ventas::find()->where(['MONTH(fecha)' => 2])->sum('saldo');
        $totalMarzo = Ventas::find()->where(['MONTH(fecha)' => 3])->sum('saldo');
        $totalAbril = Ventas::find()->where(['MONTH(fecha)' => 4])->sum('saldo');
        $totalMayo = Ventas::find()->where(['MONTH(fecha)' => 5])->sum('saldo');
        $totalJunio = Ventas::find()->where(['MONTH(fecha)' => 6])->sum('saldo');
        $totalJulio = Ventas::find()->where(['MONTH(fecha)' => 7])->sum('saldo');
        $totalAgosto = Ventas::find()->where(['MONTH(fecha)' => 8])->sum('saldo');
        $totalSeptiembre = Ventas::find()->where(['MONTH(fecha)' => 9])->sum('saldo');
        $totalOctubre = Ventas::find()->where(['MONTH(fecha)' => 10])->sum('saldo');
        $totalNoviembre = Ventas::find()->where(['MONTH(fecha)' => 11])->sum('saldo');
        $totalDiciembre = Ventas::find()->where(['MONTH(fecha)' => 12])->sum('saldo');

        // Array con los totales de compras de cada mes
        $datosGrafica = [
            $totalEnero,
            $totalFebrero,
            $totalMarzo,
            $totalAbril,
            $totalMayo,
            $totalJunio,
            $totalJulio,
            $totalAgosto,
            $totalSeptiembre,
            $totalOctubre,
            $totalNoviembre,
            $totalDiciembre,
        ];
        
        
        return $this->render('index', [
    'dataProvider' => new ActiveDataProvider([
        'query' => Ventas::find(),
    ]),
    'datosGrafica' => json_encode($datosGrafica), // Convertir el array a JSON
        ]);
    
    }
    /**
     * Displays a single Ventas model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ventas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Ventas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ventas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ventas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ventas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Ventas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ventas::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
      public function actionPdf()
{
    $searchModel = new VentasSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    $html = $this->renderPartial('pdf_view', ['dataProvider' => $dataProvider]);
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->showImageErrors = true;
    $mpdf->SetDisplayMode('fullpage', 'two');
    $mpdf->list_indent_first_level = 0;
    $mpdf->writeHTML($html);
    $mpdf->Output();
    exit;
}
}
