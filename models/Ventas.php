<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use DateTime;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property float|null $saldo
 * @property string|null $fecha
 * @property string|null $concepto
 * @property string|null $plataforma
 * @property string|null $objeto
 *
 * @property Ingresos $ingresos
 */
class Ventas extends ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['saldo', 'fecha', 'concepto', 'plataforma', 'objeto'], 'required',
                'message' => 'Este campo es obligatorio.'
            ],
            [['saldo'], 'filter', 'filter' => function ($value) {
                    return str_replace(',', '.', $value);
                }],
            [['saldo'], 'validateSaldo'],
            [['fecha'], 'match', 'pattern' => '/^(0[1-9]|[12]\d|3[01])[-\/](0[1-9]|1[0-2])[-\/]2023$/',
                'message' => 'La fecha no es válida. Asegúrese de usar el formato dd/mm/yyyy y que el año sea 2023.'
            ],
            [['fecha'], 'filter', 'filter' => function ($value) {
                    $date = \DateTime::createFromFormat('d/m/Y', $value);
                    return $date ? $date->format('Y-m-d') : null;
                }],
            [['concepto'], 'string', 'max' => 20,
                'message' => 'El concepto no puede exceder los 20 caracteres.'
            ],
            [['plataforma'], 'string', 'max' => 20,
                'message' => 'La plataforma no puede exceder los 20 caracteres.'
            ],
            [['objeto'], 'string', 'max' => 20,
                'message' => 'El objeto no puede exceder los 20 caracteres.'
            ],
        ];
    }

    public function validateSaldo($attribute, $params) {
        $value = $this->$attribute;
        // Reemplazar la coma por el punto
        $value = str_replace(',', '.', $value);
        // Verificar si el valor es un número double válido
        if (!is_numeric($value) || !preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $value)) {
            $this->addError($attribute, 'El saldo debe ser un número válido.');
            return;
        }
        // Convertir a double
        $saldoDouble = doubleval($value);
        // Verificar si está dentro del rango permitido
        if ($saldoDouble < 0 || $saldoDouble > 500000) {
            $this->addError($attribute, 'El saldo debe estar entre 0 y 500,000.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'saldo' => 'Saldo',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'plataforma' => 'Plataforma',
            'objeto' => 'Objeto',
        ];
    }

    /**
     * Gets query for [[Ingresos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngresos() {
        return $this->hasOne(Ingresos::class, ['idventa' => 'id']);
    }

    /**
     * Gets available ventas for dropdown
     * 
     * @return array
     */
    public static function getVentasDisponibles() {
        return self::find()->select(['id'])->indexBy('id')->column();
    }

}
