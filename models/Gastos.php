<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use DateTime;

class Gastos extends ActiveRecord {

    public $iva; // Agrega la propiedad iva al modelo Gastos

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'gastos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['saldo', 'fecha', 'tipo'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['saldo'], 'filter', 'filter' => function ($value) {
                    // Reemplaza las comas por puntos
                    return str_replace(',', '.', $value);
                }],
            [
                ['saldo'],
                'number',
                'min' => 0,
                'max' => 500000,
                'tooBig' => 'El valor máximo permitido es 500000. Asegúrese de que el valor no exceda este límite.',
                'tooSmall' => 'El valor mínimo permitido es 0. Asegúrese de que el valor no sea negativo.',
                'message' => 'Solo se admiten números. Asegúrese de que el valor sea un número válido y positivo.'
            ],
            [
                ['saldo'],
                'match',
                'pattern' => '/^\d+(\.|\,)?\d{0,2}$/',
                'message' => 'Solo se permiten números con hasta dos decimales.'
            ],
            [['fecha'], 'match', 'pattern' => '/^(0[1-9]|1\d|2[0-8])[-\/](0[1-9]|1[0-2])[-\/]2023$|^(29|30)[-\/](0[13-9]|1[0-2])[-\/]2023$|^(31)[-\/](0[13578]|1[02])[-\/]2023$/',
                'message' => 'La fecha no es válida. Asegúrese de usar el formato dd/mm/yyyy y que el año sea 2023.'
            ],
            [['tipo'], 'in', 'range' => ['fijo', 'variable'],
                'message' => 'El tipo debe ser "fijo" o "variable".'
            ],
            [['idobjetivofinanciero', 'idcompra'], 'integer',
                'message' => 'Este campo debe ser un número entero.'
            ],
            [['concepto'], 'string', 'max' => 20,
                'tooLong' => 'El concepto no puede exceder los 20 caracteres.',
                'message' => 'El concepto no puede exceder los 20 caracteres.'
            ],
            [['concepto'], 'match', 'pattern' => '/^(?!\s*$)(?:\S(?:\s*)){1,80}$/',
                'message' => 'El concepto no puede contener un doble espacio y debe tener como máximo 100 caracteres.'
            ],
            [['tipo'], 'string', 'max' => 10,
                'message' => 'El tipo no puede exceder los 10 caracteres.'
            ],
            [['idcompra'], 'unique',
                'message' => 'Este ID de compra ya existe.'
            ],
            [['idcompra'], 'exist', 'skipOnError' => true, 'targetClass' => Compras::class, 'targetAttribute' => ['idcompra' => 'id'],
                'message' => 'La compra especificada no existe.'
            ],
            [['fecha'], 'filter', 'filter' => function ($value) {
                    $date = \DateTime::createFromFormat('d/m/Y', $value);
                    return $date ? $date->format('Y-m-d') : null;
                }, 'skipOnEmpty' => true], // Aplicar solo si el campo no está vacío
            // Regla para la propiedad iva
            [['iva'], 'double', 'min' => 0, 'max' => 86776.86,
                'message' => 'Solo se admiten números enteros o decimales. Asegúrese de que el valor sea un número válido y esté entre 0 y 86776.86.',
                'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+\s*$/'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'saldo' => 'Precio',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'tipo' => 'Tipo',
            'idobjetivofinanciero' => 'Nombre Objetivo',
            'idcompra' => 'Nombre compra',
            'iva' => 'IVA', // Etiqueta para la propiedad iva
        ];
    }

    /**
     * Gets query for [[Idcompra0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcompra0() {
        return $this->hasOne(Compras::class, ['id' => 'idcompra']);
    }

    /**
     * Gets the related objetivo financiero for this gasto.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivofinanciero() {
        return $this->hasOne(Objetivosfinancieros::class, ['id' => 'idobjetivofinanciero']);
    }

    public static function getGastosDisponibles() {
        return self::find()->select(['id', 'concepto'])->indexBy('id')->column();
    }
    public static function obtenerMayorGasto() {
        return self::find()->orderBy(['saldo' => SORT_DESC])->one();
    }

}
