<?php

namespace app\models;

use yii\db\ActiveRecord;

class Estadisticas extends ActiveRecord
{
    public static function getTotales()
    {
        $totales = [];
        
        // Simulando valores para los cálculos
        $totales['ingresos'] = 1000; // Cálculo de los ingresos
        $totales['ventas'] = 800;    // Cálculo de las ventas
        $totales['gastos'] = 300;    // Cálculo de los gastos
        $totales['compras'] = 500;   // Cálculo de las compras
        
        return $totales;
    }
}
