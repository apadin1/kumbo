<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use DateTime;

/**
 * This is the model class for table "ingresos".
 *
 * @property int $id
 * @property float|null $saldo
 * @property string|null $fecha
 * @property string|null $concepto
 * @property string|null $emisor
 * @property int|null $idobjetivofinanciero
 * @property int|null $idventa
 *
 * @property Ventas $idventa0
 */
class Ingresos extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'ingresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['saldo', 'fecha', 'emisor', 'idobjetivofinanciero', 'idventa'], 'required',
                'message' => 'Este campo es obligatorio.'
            ],
            [['saldo'], 'filter', 'filter' => function ($value) {
                    // Reemplaza las comas por puntos
                    return str_replace(',', '.', $value);
                }],
            [['saldo'], 'number', 'min' => 0, 'max' => 500000,
                'tooBig' => 'El valor máximo permitido es 500000. Asegúrese de que el valor no exceda este límite.',
                'tooSmall' => 'El valor mínimo permitido es 0. Asegúrese de que el valor no sea negativo.',
                'message' => 'Solo se admiten números. Asegúrese de que el valor sea un número válido y positivo.'
            ],
            [['fecha'], 'match', 'pattern' => '/^(0[1-9]|1\d|2[0-8])[-\/](0[1-9]|1[0-2])[-\/]2023$|^(29|30)[-\/](0[13-9]|1[0-2])[-\/]2023$|^(31)[-\/](0[13578]|1[02])[-\/]2023$/',
                'message' => 'La fecha no es válida. Asegúrese de usar el formato dd/mm/yyyy y que el año sea 2023.'
            ],
            [['fecha'], 'filter', 'filter' => function ($value) {
                    $date = \DateTime::createFromFormat('d/m/Y', $value);
                    return $date ? $date->format('Y-m-d') : null;
                }, 'skipOnEmpty' => true], // Aplicar solo si el campo no está vacío
            [['emisor'], 'string', 'max' => 20,
                'message' => 'El emisor no puede exceder los 20 caracteres.'
            ],
            [['idobjetivofinanciero', 'idventa'], 'integer',
            ],
            [['concepto'], 'string', 'max' => 30,
                'message' => 'El concepto no puede exceder los 30 caracteres.'
            ],
            [['idventa'], 'unique',
            ],
            [['idventa'], 'exist', 'skipOnError' => true, 'targetClass' => Ventas::class, 'targetAttribute' => ['idventa' => 'id'],
                'message' => 'La venta especificada no existe.'
            ],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'saldo' => 'Saldo',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'emisor' => 'Emisor',
            'idobjetivofinanciero' => 'Nombre Objetivo',
            'idventa' => 'Numero de venta',
        ];
    }

    /**
     * Gets query for [[Idventa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdventa0() {
        return $this->hasOne(Ventas::class, ['id' => 'idventa']);
    }

    public static function obtenerMayorIngreso() {
        return self::find()->orderBy(['saldo' => SORT_DESC])->one();
    }

}
