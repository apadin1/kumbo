<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use DateTime;

class Compras extends ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['fecha', 'precio', 'proveedor'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['precio'], 'filter', 'filter' => function ($value) {
                    // Reemplaza las comas por puntos
                    return str_replace(',', '.', $value);
                }],
            [['precio'], 'validatePrecio'], // Aquí se corrigió la coma que faltaba
            [['fecha'], 'match', 'pattern' => '/^(0[1-9]|1\d|2[0-8])[-\/](0[1-9]|1[0-2])[-\/]2023$|^(29|30)[-\/](0[13-9]|1[0-2])[-\/]2023$|^(31)[-\/](0[13578]|1[02])[-\/]2023$/',
                'message' => 'La fecha no es válida. Asegúrese de usar el formato dd/mm/yyyy y que el año sea 2023.'
            ],
            [['fecha'], 'filter', 'filter' => function ($value) {
                    $date = \DateTime::createFromFormat('d/m/Y', $value);
                    return $date ? $date->format('Y-m-d') : null;
                }, 'skipOnEmpty' => true], // Aplicar solo si el campo no está vacío
            [['concepto', 'proveedor'], 'match', 'pattern' => '/^\S(.*\S)?$/', 'message' => 'Este campo no puede contener espacios dobles. Asegúrese de que el texto no tenga espacios innecesarios.'],
            [['concepto', 'proveedor'], 'string', 'max' => 20, 'tooLong' => 'Este campo no puede exceder los 20 caracteres.'],
        ];
    }

    public function validatePrecio($attribute, $params) {
        $value = $this->$attribute;
        // Reemplazar la coma por el punto
        $value = str_replace(',', '.', $value);
        // Verificar si el valor es un número double válido
        if (!is_numeric($value) || !preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $value)) {
            $this->addError($attribute, 'El precio debe ser un número válido.');
            return;
        }
        // Convertir a double
        $precioDouble = doubleval($value);
        // Verificar si está dentro del rango permitido
        if ($precioDouble < 0 || $precioDouble > 500000) {
            $this->addError($attribute, 'El precio debe estar entre 0 y 500,000.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'iva21' => 'Iva21',
            'iva10' => 'Iva10',
            'iva4' => 'Iva4',
            'concepto' => 'Concepto',
            'precio' => 'Precio',
            'fecha' => 'Fecha',
            'proveedor' => 'Proveedor',
        ];
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos() {
        return $this->hasOne(Gastos::class, ['idcompra' => 'id']);
    }

    /**
     * Gets the related objetivo financiero for this compra.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivofinanciero() {
        return $this->hasOne(Objetivosfinancieros::class, ['id' => 'idobjetivofinanciero']);
    }

    /**
     * Retorna la suma de los valores de los campos iva21, iva10 y iva4.
     *
     * @return float|null
     */
    public static function sumarIvas() {
        return self::find()
                        ->select(new Expression('COALESCE(SUM(iva21), 0) + COALESCE(SUM(iva10), 0) + COALESCE(SUM(iva4), 0)'))
                        ->scalar();
    }

    public static function getComprasDisponibles() {
        return self::find()->select(['id', 'concepto'])->indexBy('id')->column();
    }

}
