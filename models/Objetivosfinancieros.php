<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetivosfinancieros".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $objeto
 *
 * @property Precios[] $precios
 */
class Objetivosfinancieros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetivosfinancieros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
{
    return [
        [['nombre', 'objeto'], 'required',
                'message' => 'Este campo es obligatorio.'
            ],
        [['nombre', 'objeto'], 'string', 'max' => 15,
            'tooLong' => 'El nombre y el objeto no pueden exceder los 15 caracteres.', // Mensaje personalizado para longitud máxima excedida
        ],
    ];
}


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'objeto' => 'Objeto',
        ];
    }

    /**
     * Gets query for [[Precios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrecios()
    {
        return $this->hasMany(Precios::class, ['idobjetivofinanciero' => 'id']);
    }
    
    public function actionUltimoObjetivo()
    {
        // Consulta para obtener el último registro de la tabla objetivosfinancieros
        $ultimoObjetivo = Objetivosfinancieros::find()
            ->orderBy(['id' => SORT_DESC])
            ->one();

        // Verifica si se encontró algún registro
        if ($ultimoObjetivo !== null) {
            // Muestra los valores de nombre y objeto
            echo "Último nombre: " . $ultimoObjetivo->nombre . "<br>";
            echo "Último objeto: " . $ultimoObjetivo->objeto . "<br>";
        } else {
            echo "No se encontraron registros en la tabla objetivosfinancieros.";
        }
    }
    
    
    
}
