<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "precios".
 *
 * @property int $id
 * @property float|null $precio
 * @property int|null $idobjetivofinanciero
 *
 * @property Objetivosfinancieros $idobjetivofinanciero0
 */
class Precios extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'precios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['precio', 'idobjetivofinanciero'], 'required',
                'message' => 'Este campo es obligatorio.'
            ],
            [['precio'], 'filter', 'filter' => function ($value) {
                    // Reemplaza las comas por puntos
                    return str_replace(',', '.', $value);
                }],
            [['precio'], 'number', 'min' => 0, 'max' => 500000,
                'tooBig' => 'El valor máximo permitido es 500000. Asegúrese de que el valor no exceda este límite.',
                'tooSmall' => 'El valor mínimo permitido es 0. Asegúrese de que el valor no sea negativo.',
                'message' => 'Solo se admiten números. Asegúrese de que el valor sea un número válido y positivo.'
            ],
            [['idobjetivofinanciero'], 'integer',
                'message' => 'Este campo debe ser un número entero.'
            ],
            [['idobjetivofinanciero', 'precio'], 'unique', 'targetAttribute' => ['idobjetivofinanciero', 'precio'],
                'message' => 'La combinación de idobjetivofinanciero y precio ya existe.'
            ],
            [['idobjetivofinanciero'], 'exist', 'skipOnError' => true, 'targetClass' => Objetivosfinancieros::class, 'targetAttribute' => ['idobjetivofinanciero' => 'id'],
                'message' => 'El objetivo financiero especificado no existe.'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'precio' => 'Precio',
            'idobjetivofinanciero' => 'Idobjetivofinanciero',
        ];
    }

    /**
     * Gets query for [[Idobjetivofinanciero0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdobjetivofinanciero0() {
        return $this->hasOne(Objetivosfinancieros::class, ['id' => 'idobjetivofinanciero']);
    }

}
