// Para controlar la visibilidad del GridView y la imagen
$('#show-gridview-btn').on('click', function() {
    $('#gridview-container').toggle();
});
$('#show-image-btn').on('click', function() {
    $('#image-container').toggle();
});
